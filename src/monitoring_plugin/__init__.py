#!/usr/bin/python3




from .monitoring_plugin import (
                                Status,
                                exit,
                                ArgumentParser
                               )








__all__ = [
           "Status",
           "exit",
           "ArgumentParser"
          ]
















#Copyright © 2023 Christoph Anton Mitterer <mail@christoph.anton.mitterer.name>
#
#
#This program is free software: you can redistribute it and/or modify it under
#the terms of the GNU General Public License as published by the Free Software
#Foundation, either version 3 of the License, or (at your option) any later
#version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
#PARTICULAR PURPOSE.
#See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License along with
#this program. If not, see <https://www.gnu.org/licenses/>.
