#!/usr/bin/python3




import sys
import argparse
from enum import Enum








#*******************************************************************************
#*** Monitoring Plugin Interface                                             ***
#*******************************************************************************
class Status(Enum):
    OK = 0
    Warning = 1
    Critical = 2
    Unknown = 3




def exit(status, message="", performance_data_1="", details="", performance_data_2=""):
    if not isinstance(status, Status):
        (status, message, performance_data_1, details, performance_data_2) = (Status.Unknown, "Internal error: Invalid monitoring plugin status.", "", "", "")
    #TODO: Keep the following aligned with `monitoring_plugin.ArgumentParser.exit()`.
    if ("\n" in message)  or  ("|" in message):
        (status, message, performance_data_1, details, performance_data_2) = (Status.Unknown, "Internal error: Invalid character in monitoring plugin message.", "", "", "")
    if ("\n" in performance_data_1)  or  ("|" in performance_data_1):
        (status, message, performance_data_1, details, performance_data_2) = (Status.Unknown, "Internal error: Invalid character in monitoring plugin performance data 1.", "", "", "")
    if "|" in details:
        (status, message, performance_data_1, details, performance_data_2) = (Status.Unknown, "Internal error: Invalid character in monitoring plugin details.", "", "", "")
    if "|" in performance_data_2:
        (status, message, performance_data_1, details, performance_data_2) = (Status.Unknown, "Internal error: Invalid character in monitoring plugin performance data 2.", "", "", "")
    
    
    print(status.name, end="")
    if message:
        print(f": {message}", end="")
    if performance_data_1:
        print(f" | {performance_data_1}", end="")
    print("")
    
    if details:
        print(details, end="")
    if performance_data_2:
        print(f" | {performance_data_2}", end="")
    if details or performance_data_2:
        print("")
    
    
    sys.exit(status.value)




#TODO: Keep the following aligned with Python’s `argparse`-module.
class ArgumentParser(argparse.ArgumentParser):
    def _print_message(self, message, file=None):
        super()._print_message(message, sys.stderr)
    
    
    def exit(self, status=0, message=None):
        if message:
            original_message = message
            
            #extract the actual exit message
            message = message.removeprefix(f"{self.prog}: error: ").rstrip("\n")
            
            #print the original exit message if the actual exit message extracted from it won’t be (because of invalid characters)
            if ("\n" in message)  or  ("|" in message):
                self._print_message(original_message)
        else:
            #use an empty exit message
            message = "<`argparse.ArgumentParser` gave no exit message>"
        
        exit(Status.Unknown, "Command Arguments Parsing: " + message)
















#Copyright © 2023 Christoph Anton Mitterer <mail@christoph.anton.mitterer.name>
#
#
#This program is free software: you can redistribute it and/or modify it under
#the terms of the GNU General Public License as published by the Free Software
#Foundation, either version 3 of the License, or (at your option) any later
#version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
#PARTICULAR PURPOSE.
#See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License along with
#this program. If not, see <https://www.gnu.org/licenses/>.
